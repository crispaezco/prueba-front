import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CityService {

  private citiesUrl = 'localidades/ciudades';

  constructor(private http: HttpClient) { }

  getCities(): Observable<[]> {
    return this.http.post<[]>(`${environment.endpoint}${this.citiesUrl}`, {});
  }
}
