import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {Manager} from "../interfaces/manager";

@Injectable({
  providedIn: 'root'
})
export class ManagerService {

  private managerUrl = 'gestores';

  constructor(private http: HttpClient) { }

  getManagers(): Observable<Manager[]> {
    return this.http.post<Manager[]>(`${environment.endpoint}${this.managerUrl}`, {});
  }
}
