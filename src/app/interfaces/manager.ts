export interface Manager {
  id: number;
  firstName: string;
  secondName: string;
  surname: string;
  secondSurname: string;
  fullName: string;
}
