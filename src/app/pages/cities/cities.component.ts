import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {CityService} from "../../services/city.service";
import {City} from "../../interfaces/city";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})
export class CitiesComponent implements OnInit {
  displayedColumns: string[] = ['name'];
  dataSource = new MatTableDataSource<City>([]);
  count = 0;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private cityService: CityService) {
  }

  ngOnInit(): void {
    this.loadCitiesData()
  }

  loadCitiesData(): void {
    this.cityService.getCities().subscribe((data: any) => {
      this.dataSource = new MatTableDataSource<City>(data.cities);
      this.dataSource.paginator = this.paginator;
      this.count = data.count;
    })
  }

}
