import {Component, OnInit, ViewChild} from '@angular/core';
import {ManagerService} from "../../services/manager.service";
import {Manager} from "../../interfaces/manager";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";

@Component({
  selector: 'app-managers',
  templateUrl: './managers.component.html',
  styleUrls: ['./managers.component.css']
})
export class ManagersComponent implements OnInit {

  displayedColumns: string[] = ['name'];
  dataSource = new MatTableDataSource<Manager>([]);
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private managerService: ManagerService) { }

  ngOnInit(): void {
    this.loadManagers();
  }

  loadManagers(): void {
    this.managerService.getManagers().subscribe((managers: Manager[]) => {
      this.dataSource = new MatTableDataSource<Manager>(managers);
      this.dataSource.paginator = this.paginator;
    })
  }

}
