import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CitiesComponent} from "./pages/cities/cities.component";
import {ManagersComponent} from "./pages/managers/managers.component";

const routes: Routes = [
  { path: 'ciudades', component: CitiesComponent },
  { path: 'gestores', component: ManagersComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
